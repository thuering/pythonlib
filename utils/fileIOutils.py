# -*- coding: utf-8 -*-
"""
Created on Sun May 07 10:13:57 2017

@author: Thomi
"""

## IMPORTS
import codecs
import numpy as np

## FUNCIONS

def ImportTextData(filename, delimiter=' ', headerrows=0):
    
    with codecs.open(filename,'r',encoding='utf-8') as fid:
        
        lineindex = 0
        header = np.array([])
        table = np.array([])
        
        for line in fid:    
            #skip header
            if lineindex < headerrows:
                line = line.strip()
                line = ''.join(line.split())
                line = line.split(',')
                if len(header)==0:
                    header = np.array(line)
                else:
                    header = np.vstack((header, line))
            else:
                line = line.strip()
                if delimiter == ' ':
                    row = line.split()
                else:
                    row = line.split(delimiter)
                if len(table)==0:
                    table = np.float64(row)
                else:
                    table = np.vstack((table, np.float64(row)))
                
            lineindex += 1
            
    return table,header
            
    