# -*- coding: utf-8 -*-
"""
Created on Sat May 06 15:37:15 2017

@author: Thomi
"""

## IMPORTS
import os
import sys

import numpy as np
import csv
from utils import fileIOutils as fio


## FUNCTIONS

class SpecCalc(object):
    
    #Constructor
    def __init__(self, specPath, matPath, kvVal, kermaVal=1.0, filters=None):
        
        #Spectrum datapath
        self.specPath = specPath
        evec, counts = self.ImportSpectrum(specPath, kvVal, kermaVal)
        self.energyVec = evec
        self.countsVec = counts
        
        #Materials datapath
        self.matPath = matPath
        
        #Spectrum data
        self.kvVal = kvVal
        self.kermaVal = kermaVal
        self.SetAirKerma(kermaVal)
        
        self.filters = filters
        if self.filters:
            self.FilterSpectrum(self.energyVec, self.countsVec, self.filters)
        
        
    # Get Spectrum
    def GetSpectrum(self):
        return self.energyVec, self.countsVec
    
    
    
    # Import spectrum file (csv)
    def ImportSpectrum(self, datapath, kev_val, kerma_val=1.0):
        
        csvfile = os.path.join(datapath, 'spec_' + str(kev_val) + 'kV.csv')
        print(csvfile)
        if not os.path.exists(csvfile):
            raise ValueError("Spectrum file does not exist")
            
        energy_vec = np.array([])
        counts_vec = np.array([])
        
        data,header = fio.ImportTextData(csvfile, delimiter=';')
        energy_vec = data[:,0]
        counts_vec = data[:,1]
            
        counts_vec *= kerma_val
        return energy_vec, counts_vec
    
    
    def CalcAirKerma(self, energy_vec=None, counts_vec=None):
        
        if not np.any(energy_vec):
            energy_vec = self.energyVec
            
        if not np.any(counts_vec):
            counts_vec = self.countsVec
            
            
        # Calculate air kerma
        airkerma = 0
        for i_energy in range(len(energy_vec)):
            mu_en = self.GetMassEnergyAbsorptionCoef('air', energy_vec[i_energy]) #mass energy absorption coef
            airkerma += mu_en * counts_vec[i_energy] * energy_vec[i_energy] / (114.5 * 5.43e5 )
            
        return airkerma
    
    
    def SetAirKerma(self, airkerma, energy_vec=None, counts_vec=None):
        
        if not np.any(energy_vec):
            energy_vec = self.energyVec
            
        if not np.any(counts_vec):
            counts_vec = self.countsVec
            
        #Get air kerma
        airkerma_cur = self.CalcAirKerma(energy_vec, counts_vec)
        factor = airkerma / airkerma_cur
        
        counts_vec *= factor
        self.countsVec = np.copy(counts_vec)
        
        return counts_vec
                
        
    # Filter a spectrum with a given filter array
    # energy in keV
    def FilterSpectrum(self, energy_vec=None, counts_vec=None, filters=None):
        
        if not np.any(energy_vec):
            energy_vec = self.energyVec
            
        if not np.any(counts_vec):
            counts_vec = self.countsVec
            
        if not np.any(filters):
            filters = self.filters
        
        if len(energy_vec) < 1:
            print("Error: No spectrum data")
            return
        
        #Get list of filter names
        filterNames = [*filters]
        
        #wavelen_vec = 6.626e-34*3e8/(1.602e-19 * energy_vec * 1e3) #wavelength [um]
        #k_vec = 2*np.pi / wavelen_vec
        
        counts_filt_vec = np.copy(counts_vec)
        transm = np.ones(len(energy_vec))
        
        #iterate through spectrum and calculate damping vector
        for energy_i in range(len(energy_vec)):
            for filter_i in range(len(filterNames)):
                #n = self.GetRefractiveIndex(filterNames[filter_i], energy_vec[energy_i])
                #beta = n.imag
                #transm[energy_i] *= np.exp(-2 * k_vec[energy_i] * filters[filterNames[filter_i]] * beta)
                mu = self.GetLinearAbsorptionCoef(filterNames[filter_i], energy_vec[energy_i])
                transm[energy_i] *= np.exp(-mu * filters[filterNames[filter_i]] * 1e2)
        counts_filt_vec *= transm
        
        #update members
        self.filters = filters
        self.countsVec = counts_filt_vec
        self.meanEnergy = self.GetMeanEnergy()
        
        
    def GetFilterTransmission(self, energy_vec=None, filters=None):
        if not np.any(energy_vec):
            energy_vec = self.energyVec
            
        if not np.any(filters):
            filters = self.filters
        
        if len(energy_vec) < 1:
            print("Error: No spectrum data")
            return
        
        transm = np.ones(len(energy_vec))
        filterNames = [*filters]
        
        #iterate through spectrum and calculate damping vector
        for energy_i in range(len(energy_vec)):
            for filter_i in range(len(filterNames)):
                mu = self.GetLinearAbsorptionCoef(filterNames[filter_i], energy_vec[energy_i])
                transm[energy_i] *= np.exp(-mu * filters[filterNames[filter_i]] * 1e2)
        
        return transm
    
    
    
    # Get refractive index from table
    # Energy in keV
    def GetRefractiveIndex(self, material, energy):
        
        if not os.path.exists(self.matPath):
            print("Error: cannot find material tables")
            return
        
        filename = self.matPath + '/nist_' + material + '.dat'
        data,header = fio.ImportTextData(filename, delimiter=' ',headerrows=1)
        
        # Find indices
        ind_energy = header.tolist().index('energy')
        ind_delta = header.tolist().index('delta')
        ind_beta = header.tolist().index('beta')
        
        # Get data
        energy_vec = data[:,ind_energy]
        delta_vec = data[:,ind_delta]
        beta_vec = data[:,ind_beta]
        
        # Check if requested energy is within range
        if energy < energy_vec[0] or energy > energy_vec[-1]:
            print("Error: requested energy not in tabulated range")
            return
        
        # Interpolate in log-log domain
        delta_out = np.exp(np.interp(np.log(energy), np.log(energy_vec), np.log(delta_vec)))
        beta_out = np.exp(np.interp(np.log(energy), np.log(energy_vec), np.log(beta_vec)))
        
        # Complex refractive index: n = 1 - delta + j*beta
        n = 1 - delta_out + beta_out * 1j
        
        return n
    
    # Get refractive index from table
    # Energy in keV
    def GetLinearAbsorptionCoef(self, material, energy):
        
        if not os.path.exists(self.matPath):
            print("Error: cannot find material tables")
            return
        
        filename = self.matPath + '/nist_' + material + '.dat'
        data,header = fio.ImportTextData(filename, delimiter=' ',headerrows=1)
        
        # Find indices
        ind_energy = header.tolist().index('energy')
        ind_mu = header.tolist().index('mu')
        
        # Get data
        energy_vec = data[:,ind_energy]
        mu_vec = data[:,ind_mu]
        
        # Check if requested energy is within range
        if energy < energy_vec[0] or energy > energy_vec[-1]:
            print("Error: requested energy not in tabulated range")
            return
        
        # Interpolate in log-log domain
        mu_out = np.exp(np.interp(np.log(energy), np.log(energy_vec), np.log(mu_vec)))
        
        return mu_out
        
    
    def GetMassAttenuationCoef(self, material, energy):
        
        if not os.path.exists(self.matPath):
            print("Error: cannot find material tables")
            return
        
        filename = self.matPath + '/nist_' + material + '.dat'
        data,header = fio.ImportTextData(filename, delimiter=' ',headerrows=1)
        
        # Find indices
        ind_energy = header.tolist().index('energy')
        ind_mu_tot = header.tolist().index('total')
        
        # Get data
        energy_vec = data[:,ind_energy]
        mu_tot_vec = data[:,ind_mu_tot]
        
        # Check if requested energy is within range
        if energy < energy_vec[0] or energy > energy_vec[-1]:
            print("Error: requested energy not in tabulated range")
            return
        
        mu_tot_out = np.exp(np.interp(np.log(energy), np.log(energy_vec), np.log(mu_tot_vec)))
        
        return mu_tot_out
    
    
    def GetMassEnergyAbsorptionCoef(self, material, energy):
        
        if not os.path.exists(self.matPath):
            print("Error: cannot find material tables")
            return
        
        filename = self.matPath + '/nist_' + material + '.dat'
        data,header = fio.ImportTextData(filename, delimiter=' ',headerrows=1)
        
        # Find indices
        ind_energy = header.tolist().index('energy')
        ind_mu_en = header.tolist().index('mu_en')
        
        # Get data
        energy_vec = data[:,ind_energy]
        mu_en_vec = data[:,ind_mu_en]
        
        # Check if requested energy is within range
        if energy < energy_vec[0] or energy > energy_vec[-1]:
            print("Error: requested energy not in tabulated range")
            return
        
        mu_en_out = np.exp(np.interp(np.log(energy), np.log(energy_vec), np.log(mu_en_vec)))
        
        return mu_en_out
    
    
    def GetMassPhotoAbsorptionCoef(self, material, energy):
        
        if not os.path.exists(self.matPath):
            print("Error: cannot find material tables")
            return
        
        filename = self.matPath + '/nist_' + material + '.dat'
        data,header = fio.ImportTextData(filename, delimiter=' ',headerrows=1)
        
        # Find indices
        ind_energy = header.tolist().index('energy')
        ind_photo = header.tolist().index('photo')
        
        # Get data
        energy_vec = data[:,ind_energy]
        mu_photo = data[:,ind_photo]
        
        # Check if requested energy is within range
        if energy < energy_vec[0] or energy > energy_vec[-1]:
            print("Error: requested energy not in tabulated range")
            return
        
        mu_photo_out = np.exp(np.interp(np.log(energy), np.log(energy_vec), np.log(mu_photo)))
        
        return mu_photo_out
    
    
    def GetMassScatterAbsorptionCoef(self, material, energy):
        
        if not os.path.exists(self.matPath):
            print("Error: cannot find material tables")
            return
        
        filename = self.matPath + '/nist_' + material + '.dat'
        data,header = fio.ImportTextData(filename, delimiter=' ',headerrows=1)
        
        # Find indices
        ind_energy = header.tolist().index('energy')
        ind_scatter = header.tolist().index('scatter')
        
        # Get data
        energy_vec = data[:,ind_energy]
        mu_scatter = data[:,ind_scatter]
        
        # Check if requested energy is within range
        if energy < energy_vec[0] or energy > energy_vec[-1]:
            print("Error: requested energy not in tabulated range")
            return
        
        mu_scatter_out = np.exp(np.interp(np.log(energy), np.log(energy_vec), np.log(mu_scatter)))
        
        return mu_scatter_out
    

    
    def GetMeanEnergy(self, energy_vec=None, counts_vec=None):
        
        if not np.any(energy_vec):
            energy_vec = self.energyVec
            
        if not np.any(counts_vec):
            counts_vec = self.countsVec
            
        mean_energy = np.sum(counts_vec * energy_vec) / np.sum(counts_vec)
        
        return mean_energy