# -*- coding: utf-8 -*-
"""
Created on Wed Nov 01 15:05:50 2017

@author: thomas.thuering
"""

## IMPORTS
import os
import sys
import glob
import json

import numpy as np
import csv
from utils import fileIOutils as fio

import matplotlib.pyplot as plt

from pcdsimlib import specCalc as spec


## CONSTANTS
DATAPATH = os.path.join('data','pcd_response')
FILEPREFIX = 'energy_scan'

# specCalc Paths
path_spectra = os.path.join('data','xray_spectra')
path_materials = os.path.join('data','nist_tables')

## FUNCTIONS

class PcdSim(object):
    
    #Constructor
    def __init__(self, a_erange, a_sensorMaterial, a_sensorThickness, a_pixelSize):
        self.erange = a_erange
        self.pixelSize = a_pixelSize
        self.datapath = DATAPATH + '/' + a_sensorMaterial + '_' + str(a_sensorThickness) + '_' + str(a_pixelSize)
        
        self.sensorMaterial = a_sensorMaterial
        self.sensorThickness = a_sensorThickness
        spectrum = spec.SpecCalc(path_spectra, path_materials, kvVal=50, filters={a_sensorMaterial:a_sensorThickness*1e-6})
        self.sensorAbsorption = 1 - spectrum.GetFilterTransmission(energy_vec=a_erange)
       
        
    def GetResponseMatrixFull(self, a_erange=None):
        # Check input parameters
        if not np.any(a_erange):
            a_erange = self.erange
        
        # Check if response functions are available
        respMat = np.zeros((len(a_erange), len(a_erange)))
        e_min = a_erange[0]
        e_max = a_erange[-1]
        
        #plt.figure()
        for i_energy in range(len(a_erange)):
            energy = a_erange[i_energy]
            fname = self.__GetResponseFunctionFilename(energy)
            if not os.path.isfile(fname):
                print('ERROR: Response function not found: ' + fname)
                return
            
            # Load function
            x,y = self.__GetResponseFunctionFromFile(fname)
            
            # Crop energy axis and response functions and normalize
            x2 = np.uint(np.ceil(x * np.double(energy)))
            x3 = x2[np.logical_and(x2 >= e_min - 0.001, x2 <= e_max + 0.001)]
            y2 = y[np.logical_and(x2 >= e_min - 0.001, x2 <= e_max + 0.001)]
            y3 = y2 / np.sum(y2) * self.sensorAbsorption[i_energy]
            #plt.plot(x3, y3)
            
            respMat[:,i_energy] = y3[:]
                
        return respMat
            
        
    def GetResponseMatrixBinned(self, a_thr):
        
        resp_mat = self.GetResponseMatrixFull()
        
        #sum rows only
        resp_mat_thr = np.zeros((len(a_thr),resp_mat.shape[1]))
        for i_thr in range(len(a_thr) - 1):
            erange_ind = np.logical_and(self.erange >= a_thr[i_thr],self.erange < a_thr[i_thr + 1])
            resp_mat_thr[i_thr,:] = np.sum(resp_mat[erange_ind,:], axis=0)
        resp_mat_thr[-1,:] = np.sum(resp_mat[self.erange >= a_thr[-1],:], axis=0)
        
        return resp_mat_thr
        
    
    def GetResponseMatrixBinned2(self, a_thr):
        
        #sum rows
        resp_mat_thr_tmp = self.GetResponseMatrixBinned(a_thr)
        
        #in addition sum columns
        resp_mat_thr = np.zeros((len(a_thr),len(a_thr)))
        for j_thr in range(len(a_thr) - 1):
            erange_ind = np.logical_and(self.erange >= a_thr[j_thr],self.erange < a_thr[j_thr + 1])
            resp_mat_thr[:,j_thr] = np.sum(resp_mat_thr_tmp[:,erange_ind], axis=1)
        resp_mat_thr[:,-1] = np.sum(resp_mat_thr_tmp[:,self.erange >= a_thr[-1]], axis=1)
        
        return resp_mat_thr

            
    def __GetResponseFunctionFilename(self, a_energy):
        return self.datapath + '/' + FILEPREFIX + '_'  \
    + self.sensorMaterial.lower() + '_' \
    + str(self.pixelSize).lower() + 'um_' + str(a_energy) + '.json'
    
    
    def __GetResponseFunctionFromFile(self, a_filename):
        if not os.path.isfile(a_filename):
            print('ERROR: Cannot find file')
            return
        
        fp = open(a_filename)
        data = json.load(fp)
        fp.close()
        
        return np.array(data['x']), np.array(data['y'])
        
        