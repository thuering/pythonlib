from __future__ import division, print_function

from scipy.constants import N_A, physical_constants, pi
from string import Template
from bs4 import BeautifulSoup
import os
import urllib.request as urr
import math
import codecs
import periodicTable
import sys

template_url = Template(
        "http://physics.nist.gov/cgi-bin/ffast/ffast.pl?\
Z=$Z&Formula=$F&gtype=4&range=S\
&lower=$lower_energy&upper=$upper_energy\
&density=&frames=no&htmltable=1")

class ScatteringFactorTable(object):
    """get and save the scattering factors table for given
    element/formula and energy range (keV)
    calculates delta and beta with formula (1) in section 1.7 of the xray
    data booklet http://xdb.lbl.gov/"""
    def __init__(self, chemical, alias_name, density=None, min_energy=1, max_energy=200, folder="nist"):
        super(ScatteringFactorTable, self).__init__()
        self.min_energy = min_energy
        self.max_energy = max_energy
        
        if alias_name:
            self.file_name = os.path.join(folder, "nist_{0}.dat".format(alias_name))
        else:
            self.file_name = os.path.join(folder, "nist_{0}.dat".format(chemical))
        
        # Check if chemical is element name
        if chemical in periodicTable.name_to_Z.keys():
            self.element = periodicTable.name_to_Z[chemical]
            self.formula = ""
            self.density = None
        else:
            self.element = ""
            self.formula = chemical
            if not density:
                print('Error: ScatteringFactorTable: need density for compound material')
                sys.exit(1)
                
            self.density = density
            
        # Set NIST URL
        self.url = template_url.safe_substitute(
                Z=self.element,
                F=self.formula,
                lower_energy=min_energy,
                upper_energy=max_energy
                )
        

    def get_density(self, text):
        for line in text.splitlines():
            if "Nominal density" in line.decode("utf-8"):
                line = line.split()
                atomic_weight = float(line[0])
                self.density = float(line[-1])
                """convert to cm to be consistent with the other NIST tables"""
                self.atoms_per_unit_volume = N_A * self.density / atomic_weight
                break
    
                
    def get_table(self):
        page = urr.urlopen(self.url)
        text = page.read()
        soup = BeautifulSoup(text)
        table = soup.find("table")
        rows = table.findAll("tr")
        self.table = []
        for row in rows:
            cols = row.findAll("td")
            if not cols: continue
            #cols = cols[:3] + [cols[-1]]
            table_row = []
            for cell in cols:
                content = cell.find(text=True).replace("&nbsp;", "")
                table_row.append(content)
            self.table.append(table_row)
            
        if self.element:
            self.get_density(text)

    def save_table(self):
        self.get_table()
        with codecs.open(self.file_name, "w", encoding='utf8') as out_file:
            if self.element:
                print("energy, f1, f2, photo, scatter, total, mu_en, wavelength, delta, beta, mu", file=out_file)
           
                """convert to cm to be consistent with the other NIST tables"""
                r_e = physical_constants["classical electron radius"][0] * 1e2
                factor = r_e / (2 * pi) * self.atoms_per_unit_volume
                for row in self.table:
                    #add all native rows
                    for column in row:
                        print(column, end=" ", file=out_file)
                        
                    #add delta, beta
                    f1 = float(row[1])
                    f2 = float(row[2])
                    wavelength = float(row[-1]) * 1e-7
                    delta = factor * wavelength * wavelength * f1
                    beta = factor * wavelength * wavelength * f2
                    print(delta, end=" ", file=out_file)
                    print(beta, end=" ", file=out_file)
                    
                    #add mu
                    total = float(row[-3]) #total mass attentuation coeff
                    mu = total * self.density
                    print(mu, file=out_file)
                
            elif self.formula:
                print("energy, f2, photo, total, mu", file=out_file)
             
                for row in self.table:
                    #add all native rows
                    for column in row:
                        print(column, end=" ", file=out_file)
                        
                    #add mu
                    total = float(row[-1]) #total mass attentuation coeff
                    mu = total * self.density
                    print(mu, file=out_file)