# -*- coding: utf-8 -*-
"""
Created on Sat May 06 16:39:33 2017

@author: Thomi
"""

## IMPORTS
import nistLookup

## PARAMETERS
#chemical = 'C18H24I3N3O8'
#alias_name = 'Iopromide'
#density = 1.164 # only for compounds [g/cm^3], ignored for element

#chemical = 'CdTe'
#alias_name = 'cdte'
#density = 5.85 # only for compounds [g/cm^3], ignored for element

#chemical = 'GaAs'
#alias_name = 'gaas'
#density = 5.3176 # only for compounds [g/cm^3], ignored for element

#chemical = 'C18H24I3N3O8'
#alias_name = 'Ultravist150'
#density = 0.3117 # only for compounds [g/cm^3], ignored for element
##Ultravist 150:
## - Ultravist: Solution of Iopromide and Water
## - Ultravist150: Solution contains 311.7mg iopromide per ml
## - Solution total density: 1164mg/ml
## - Volume of Iopromide per ml solution: V = M / rho: V = 311.7mg / 1164mg/ml = 0.268 ml Iopromide per ml solution (i.e. 0.732 ml buffer)
## - Iopromide total content in solution: 26.8%
## - Divide Iopromide density by 1/0.268 = 3.7344
## - Density = 1164mg/ml / 3.7344 = 311.7mg/ml



#chemical = 'C18H24I3N3O8'
#alias_name = 'Ultravist370'
#density = 0.76886 # only for compounds [g/cm^3], ignored for element
##Ultravist 370:
## - Ultravist: Solution of Iopromide and Water
## - Ultravist370: Solution contains 768.86mg iopromide per ml
## - Solution total density: 1409mg/ml
## - Volume of Iopromide per ml solution: V = M / rho: V = 768.86mg / 1409mg/ml = 0.5457 ml Iopromide per ml solution (i.e. 0.732 ml buffer)
## - Iopromide total content in solution: 54.57%
## - Divide Iopromide density by 1/0.54.57 = 1.8326
## - Density = 1409mg/ml / 1.8326 = 768.86 mg/ml


chemical = 'Ca5HO13P3'
alias_name = 'ha'
density = 3.14

path_output = r'C:\Users\thomas.thuering\python\pythonlib\pcdsimlib\data\nist_tables'

## PROCESSING
a = nistLookup.ScatteringFactorTable(chemical, alias_name, density, 1, 200,path_output)
a.save_table()
