# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 14:14:16 2017

@author: thomas.thuering
"""

## module gaussPivot
''' x = gaussPivot(a,b,tol=1.0e-9).
    Solves [a]{x} = {b} by Gauss elimination with
    scaled row pivoting
    Taken from the book Numerical Methods in Engineering with Python
    by J. Kiusalaas    
'''
from numpy import *
import numpy as np
import swap    
import error

def gaussPivot(a,b,tol=1.0e-12):
	n = len(b)
	
	# Set up scale factors
	s = np.zeros(n)
	for i in range(n):
		s[i] = max(abs(a[i,:]))
	
	for k in range(0,n-1):
		# Row interchange, if needed
		p = int(argmax(abs(a[k:n,k])/s[k:n])) + k
		if abs(a[p,k]) < tol: error.err('Matrix is singular')
		if p != k:
			swap.swapRows(b,k,p)
			swap.swapRows(s,k,p)
			swap.swapRows(a,k,p)
		
		# Elimination
		for i in range(k+1,n):
			if a[i,k] != 0.0:
				lam = a[i,k]/a[k,k]
				a[i,k+1:n] = a [i,k+1:n] - lam*a[k,k+1:n]
				b[i] = b[i] - lam*b[k]
	if abs(a[n-1,n-1]) < tol: error.err('Matrix is singular')
	
	# Back substitution
	for k in range(n-1,-1,-1):
		b[k] = (b[k] - np.dot(a[k,k+1:n],b[k+1:n]))/a[k,k]
	return b



def newtonRaphson2(f,x,tol=1.0e-5):
	
	def jacobian(f,x):
		h = 1.0e-4
		n = len(x)
		jac = np.zeros((n,n))
		f0 = f(x)
		for i in range(n):
			temp = x[i]
			x[i] = temp + h
			f1 = f(x)
			x[i] = temp
			jac[:,i] = (f1 - f0)/h
		return jac,f0
	
	for i in range(300):
		jac,f0 = jacobian(f,x)
		if np.sqrt(np.dot(f0,f0)/len(x)) < tol: return x
		#print i, jac, -f0
		dx = gaussPivot(jac,-f0)
		x = x + dx
		if np.sqrt(np.dot(dx,dx)) < tol*max(max(abs(x)),1.0): return x
	print 'Too many iterations'
