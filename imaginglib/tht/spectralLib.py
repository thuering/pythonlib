# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 12:39:25 2017

@author: tthuering
"""
import numpy as np
import cv2
import imaginglib.tht.imgLib as iml

#border: border pixels which should be interpolated
#maskMargin: additional pixels to normal mask which shall be interpolated
def PreprocessDectrisRawImage(img, border=2, maskMargin=1):
    img1 = np.copy(img)
    
    #get dead pixel mask
    mask1 = img1 == 0
    
    #extend mask by maskMargin
    mask2 = cv2.boxFilter(np.int32(mask1), ddepth=-1, ksize=(1+2*maskMargin,1+2*maskMargin), normalize=False, borderType=cv2.BORDER_REFLECT)
    mask3 = mask2 > 0
    
    #add border
    mask3[0:border,:] = 1
    mask3[-border:,:] = 1
    mask3[:,0:border] = 1
    mask3[:,-border:] = 1
         
    mask4 = ~mask3 #invert
    img1 = img1 * mask4
    
    #image and mask convolution with 3x3 kernel
    img1c = cv2.boxFilter(img1, ddepth=-1, ksize=(3+2*maskMargin,3+2*maskMargin), normalize=False, borderType=cv2.BORDER_REFLECT)
    maskc = cv2.boxFilter(np.int32(mask4), ddepth=-1, ksize=(3+2*maskMargin,3+2*maskMargin), normalize=False, borderType=cv2.BORDER_REFLECT)
    maskc[maskc==0] = 1
         
    #normalize corrected image
    img1c = img1c * 1/maskc
    
    #replace corrected values in gaps
    img_out = np.copy(img)
    img_out[mask3] = img1c[mask3]
    
    return img_out


def PreprocessDectrisCorrectedImage(img):
    img1 = np.copy(img)
    
    #find entries with high values
    mask1 = img1 > 1.2
    
    #percentile out high values
    img_s = np.sort(img1.flatten())
    
    #get threshold from 95% lower image pixels
    thr = np.max(img_s[0:np.round(np.size(img)*0.99)])
    
    mask2 = img > 1.5*thr
    
    #get total mask
    mask = np.logical_or(mask1, mask2)
    
    #delete those entries
    img1[mask] = 0
    
    #image and mask convolution with 3x3 kernel
    img1c = cv2.boxFilter(img1, ddepth=-1, ksize=(3,3), normalize=True, borderType=cv2.BORDER_REFLECT)
    
    #replace corrected valuies in gaps
    img_out = np.copy(img)
    img_out[mask] = img1c[mask]
    
    return img_out


#roiNoise: [xstart,ystart,width,height]
def CorrectOutliers(img, roiNoise, corrZero=False):
    img1 = np.copy(img)
    
    xstart = roiNoise[0]
    ystart = roiNoise[1]
    width = roiNoise[2]
    height = roiNoise[3]
    
    #measure noise
    noise = np.std(img1[ystart:ystart+height, xstart:xstart+width])
    med = np.median(img1[ystart:ystart+height, xstart:xstart+width])
    
    #correct outliers
    mask = np.logical_or(img1 > med + 5*noise, img1 < med - 5*noise)
        
    #correct zeros
    if corrZero:
        mask = np.logical_or(mask, img==0)
        
    img1[mask] = med
    return img1, mask
    


def GetSpectralBinImage(img, binInd, ff=None):
    img1 = np.copy(img)
    ff1 = None
    
    if np.any(ff):
        if img1.shape != ff.shape:
            raise Exception('GetSpectralBinImage(): shape of img and ff must match')
            return
        ff1 = np.copy(ff)
    
    if np.ndim(img1)==2:
        return img1
    
    if len(binInd) > 2:
        raise Exception('GetSpectralBinImage(): binInd has bad shape')
        return
    
    #threshold image
    if len(binInd)==1:
        img_bin = img1[:,:,binInd[0]]
        
        if np.any(ff1):
            ff_bin = ff1[:,:,binInd[0]]
        else:
            ff_bin = 1
            
        img_out = img_bin / ff_bin
        
        #remove 
        return img_out
        
    #do not use this function down here
    img_bin = img1[:,:,binInd[0]] - img1[:,:,binInd[1]]
    
    if np.any(ff1):
        ff_bin = ff1[:,:,binInd[0]] - ff1[:,:,binInd[1]]
    else:
        ff_bin = 1
    
    img_out = img_bin / ff_bin
    
    return img_out


def GetSpectralImages(img, ff=None):
    img1 = np.copy(img)
    ff1 = None
    
    if np.any(ff):
        if img1.shape != ff.shape:
            raise Exception('GetSpectralImages(): shape of img and ff must match')
            return
        ff1 = np.copy(ff)
    
    if np.ndim(img1)==2:
        return img1
    
    #number of thresholds
    nthr = img1.shape[2]
    
    img_out = np.zeros(img1.shape)
    
    for i_thr in range(nthr-1):
        img_bin = img1[:,:,i_thr] - img1[:,:,i_thr+1]
        if np.any(ff1):
            ff_bin = ff1[:,:,i_thr] - ff1[:,:,i_thr+1]
            
        else:
            ff_bin = 1
        
        img_tmp = img_bin / ff_bin
        img_tmp[img_tmp==np.inf] = 1.0
        
        img_out[:,:,i_thr] = img_tmp
            
      
    img_bin = img1[:,:,nthr-1]
    if np.any(ff1):
        ff_bin = ff1[:,:,nthr-1]
    else:
        ff_bin = 1
        
    img_tmp = img_bin / ff_bin
    img_tmp[img_tmp==np.inf] = 1.0
    
    img_out[:,:,nthr-1] = img_tmp
    
    return img_out
    
    
    
    