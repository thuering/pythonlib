# -*- coding: utf-8 -*-
"""
Created on Tue Jun 07 15:13:29 2016

@author: tthuering
"""

## IMPORTS
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import cv2

## CLASSES

## FUNCTIONS

#  User defined imshow function
def imshow(img, clim=0, cb=True, cmap=cm.gray, newfig=True, hover=True, realtime=False):
        
    if newfig and not realtime:
        h_plt = plt.figure()
    else:
        h_plt = plt.gcf()
    
    if not clim:
        #first remove nans if there are any
        img2 = img[np.invert(np.isnan(img))]
        clim = [np.min(img2), np.max(img2)]
        
    #fig, h_ax = plt.subplots()
    h_plt.clf()
    plt.imshow(img, cmap=cmap, clim=clim, interpolation='nearest')
    h_ax = h_plt.get_axes()[0] #get axis handle
    
    if cb:
        plt.colorbar()

    numrows, numcols = img.shape
    
    def format_coord(x, y):
        col = int(x + 0.5)
        row = int(y + 0.5)
        if col >= 0 and col < numcols and row >= 0 and row < numrows:
            z = img[row, col]
            return 'x=%d, y=%d, z=%.5f' % (x, y, z)
        else:
            return 'x=%d, y=%d' % (x, y)
            
    h_ax.format_coord = format_coord
    h_plt.canvas.draw()

    if realtime:
        plt.pause(0.01)
        
    return h_plt, h_ax
        
#rebin
#img: input image
#b: binning factor (integer)
def rebin(img, b):
    
    if np.ndim(img)==1:
        img = np.reshape(img,(1,len(img)))
        img_b = rebinHorizontal(img, b)
        return img_b.flatten()

    #vertical binning:
    img_v = rebinVertical(img, b)
    
    #horizontal binning:
    img_vh = rebinHorizontal(img_v, b)
    
    if oneDim:
        img_vh = img_vh.flatten()
    
    return img_vh
            
            
def rebinVertical(img, b):
    
    if b==1:
        return np.copy(img)
    
    N = img.shape
    r = np.mod(N[0], b)
    if r:
        tmp = np.zeros(((N[0]-r)/b,N[1]))
        for j in range(b):
            tmp = tmp + img[j:-r:b,:]
		
    else:
        tmp = np.zeros((N[0]/b,N[1]))
        for j in range(b):
            tmp = tmp + img[j::b,:]
	
    res = tmp / b
    
    return res
    

def rebinHorizontal(img, b):
    
    if b==1:
        return np.copy(img)
    
    N = img.shape
    r = np.mod(N[1], b)
    if r:
        tmp = np.zeros((N[0],(N[1]-r)/b))
        for j in range(b):
            tmp = tmp + img[:,j:-r:b]
		
    else:
        tmp = np.zeros((N[0],N[1]/b))
        for j in range(b):
            tmp = tmp + img[:,j::b]
    
    res = tmp / b
    
    return res
    
    
    
# cropImage(img, cp)
 # Crop image using 4 crop parameters
 # cp: crop parameter vector containing 4 elements [left, right, top, bottom]
def CropImage(img, cp):
    img_out = np.copy(img)
    
    if cp[0]:
        img_out = img_out[:,cp[0]:]
    if cp[1]:
        img_out = img_out[:,:-cp[1]]
    if cp[2]:
        img_out = img_out[cp[2]:,:]
    if cp[3]:
        img_out = img_out[:-cp[3],:]
    
    return img_out
    
def NormalizeImage(img):
    tmp = img - np.min(img)
    return tmp / np.max(tmp)
    
# zeroPad(img, zpv)
 # left right zero padding with zero pad relative value zpr
 # if zpr = c: relative padding: the image is padded left and right with c * img_width zeroes each
 # if zpr = [l,r,t,b]: absolute padding left, righ, top, bottom
def zeroPad(img, zpr):
    if len(zpr)==1:
        zpad = np.floor(img.shape[1] * zpr)
        img_out = np.pad(img, ((0,0),(zpad,zpad)), mode='constant')
    elif len(zpr)==4:
        img_out = np.pad(img, ((zpr[2],zpr[3]),(zpr[0],zpr[1])), mode='constant')
    else:
        return -1
    return img_out
    
    
def unsharpMasking(img, GaussKernelSize=(9,9), GaussKernelSigma=5, SubtractionWeight=0.5):
     img_g = cv2.GaussianBlur(img, ksize=GaussKernelSize, sigmaX=GaussKernelSigma, sigmaY=GaussKernelSigma)
     img_out = cv2.addWeighted(img, 1.0, img_g, -SubtractionWeight, 0)
     return img_out
     
     
# GetLaplacianPyramidLevel(img, level)
# Get Gaussian low pass and laplacian high pass image of specific laplacian
# pyramid level
def GetLaplacianPyramidLevel(img, level):
    # get laplacian images for next level
    img_low, img_high = GetLaplacianPyramidImages(img)
    
    # get next level recursively
    if level-1==0:
        return img_low, img_high
    else:
        return GetLaplacianPyramidLevel(img_low,level-1)
        
    
# MultiResolutionImageEnhancement(img, nLevels)
# Do multi resolution image processing on image
# img: input image
# nLevels: number of levels to descend
def GetLPLevelProcessedImage(img, totLevels, level, contrastSlope, fEdge, nEdge, fLatitude, nLatitude):
        
    if level==0:
        return img
    
    # get gaussian and laplacian images for this level
    img_low, img_high = GetLaplacianPyramidImages(img)
    
    # Coefficient processing of laplacian image for this level
    
    # Denoise
    #img_high = NLMDenoising(img_high, 3.0, 7, 21)
#    if level==totLevels:
#        img_high = solve_TVL1(img_high, 10.0)
    
    # Contrast equalization
    img_high = AmplifyContrast(img_high, contrastSlope)
    
    # Edge enhancement
    if level > totLevels - nEdge:
        a_edge = fEdge**(1-(totLevels-level)/nEdge)
        img_high = img_high * a_edge
        
    # Latitude reduction
    if level <= nLatitude:
        a_lat = fLatitude**((level-nLatitude-1)/nLatitude)
        img_high = img_high * a_lat
        
    # get next level result image recursively
    img_res = GetLPLevelProcessedImage(img_low, totLevels, level-1, contrastSlope, fEdge, nEdge, fLatitude, nLatitude)
    
    # upsample and add to laplace image
    img_res_high = cv2.pyrUp(img_res)
    img_high, img_res_high = PadToSameSize(img_high, img_res_high)
    img_high_2 = cv2.add(img_high, img_res_high)
    
    return img_high_2
    
    
    
# GetLaplacianPyramidImages(img)
# Get Gaussian low pass and laplacian high pass image of next laplacian pyramid
# level
def GetLaplacianPyramidImages(img):
   
    # calculate laplacian pyramid level images
    img_low = cv2.pyrDown(img)
    tmp = cv2.pyrUp(img_low)
    img, tmp = PadToSameSize(img, tmp)
    img_high = cv2.subtract(img, tmp)
    
    return img_low, img_high
    
    
def PadToSameSize(img1, img2):
    # Adjust vertical dimensions
    offset = np.shape(img2)[0] - np.shape(img1)[0]
    if offset < 0:
        img2 = cv2.copyMakeBorder(img2,0,-offset,0,0,cv2.BORDER_REPLICATE)
    elif offset > 0:
        img2 = CropImage(img2, [0,0,0,offset])
        
    # Adjust horizontal dimensions
    offset = np.shape(img2)[1] - np.shape(img1)[1]
    if offset < 0:
        img2 = cv2.copyMakeBorder(img2,0,0,0,-offset,cv2.BORDER_REPLICATE)
    elif offset > 0:
        img2 = CropImage(img2, [0,offset,0,0])
        
    return img1, img2
    

def AmplifyContrast(img, p):
    a = np.max(np.abs(img))
    img = img/a
    img[img==0] = 0.001
    return a * img * np.power(np.abs(img), p-1)