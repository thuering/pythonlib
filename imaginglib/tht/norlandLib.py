# -*- coding: utf-8 -*-
"""
Created on Wed May 06 17:52:46 2015

@author: tthuering

File I/O library for Norland images
"""

#Imports
import numpy
import struct

"""
readSCD(filename, img_id = 0)
Reads SCM files produced by Norland scanners
Inputs:
 filename: Input file
 img_id: 0: metal, 1:plastic
Output:
 image: image data
 param: header parameters
"""
def readSCD(filename,img_id = 0):

    #Processing
    with open(filename, mode='rb') as fid:
    
        param = {}
        param['fid_id'] = struct.unpack('<H', fid.read(2))[0]
        param['minor_ver'] = struct.unpack('<B', fid.read(1))[0]
        param['major_ver'] = struct.unpack('<B', fid.read(1))[0]
        param['buf_siz'] = struct.unpack('<H', fid.read(2))[0]
        param['scnr_rev'] = struct.unpack('<H', fid.read(2))[0]
        param['soft_ver'] = struct.unpack('<H', fid.read(2))[0]
        
        param['numLine'] = struct.unpack('<H', fid.read(2))[0]
        param['numPts'] = struct.unpack('<H', fid.read(2))[0]
        
        param['pnt_res'] = struct.unpack('<H', fid.read(2))[0]
        param['lin_res'] = struct.unpack('<H', fid.read(2))[0]
        param['scn_wid'] = struct.unpack('<H', fid.read(2))[0]
        param['scn_spd'] = struct.unpack('<H', fid.read(2))[0]
        param['ini_x'] = struct.unpack('<h', fid.read(2))[0]
        param['ini_y'] = struct.unpack('<h', fid.read(2))[0]
        param['fin_x'] = struct.unpack('<h', fid.read(2))[0]
        param['fin_y'] = struct.unpack('<h', fid.read(2))[0]
        param['bas_met'] = struct.unpack('<h', fid.read(2))[0]
        param['bas_pla'] = struct.unpack('<h', fid.read(2))[0]
        param['bas_r'] = struct.unpack('<i', fid.read(4))[0]/1000000000
        param['fat_th'] = struct.unpack('<h', fid.read(2))[0]
        param['nbone_th'] = struct.unpack('<h', fid.read(2))[0]
        param['bont_th'] = struct.unpack('<h', fid.read(2))[0]
        param['soft_r'] = struct.unpack('<i', fid.read(4))[0]/1000000000
        param['bon_r'] = struct.unpack('<i', fid.read(4))[0]/1000000000
        param['bmc_min'] = struct.unpack('<h', fid.read(2))[0]
        param['bmc_max'] = struct.unpack('<h', fid.read(2))[0]
        param['filter'] = struct.unpack('<h', fid.read(2))[0]
        param['r'] = struct.unpack('<i', fid.read(4))[0]/1000000000
        param['cal_dat'] = struct.unpack('<I', fid.read(4))[0]
        param['cal_tim'] = struct.unpack('<I', fid.read(4))[0]
        param['bas_x'] = struct.unpack('<H', fid.read(2))[0]
        param['bas_y'] = struct.unpack('<H', fid.read(2))[0]
        param['poly'] = struct.unpack('<h', fid.read(2))[0]
        param['fin_reas'] = struct.unpack('<H', fid.read(2))[0]
        param['back_hi'] = struct.unpack('<I', fid.read(4))[0]
        param['back_lo'] = struct.unpack('<I', fid.read(4))[0]
        param['arcs'] = struct.unpack('<H', fid.read(2))[0]
        param['extreme'] = struct.unpack('<H', fid.read(2))[0]
        param['date_'] = struct.unpack('<I', fid.read(4))[0]
        param['time_'] = struct.unpack('<I', fid.read(4))[0]
        param['scn_typ'] = struct.unpack('<H', fid.read(2))[0]
        param['tbl_met'] = struct.unpack('<h', fid.read(2))[0]
        param['tbl_pla'] = struct.unpack('<h', fid.read(2))[0]
        param['metal_detect'] = struct.unpack('<H', fid.read(2))[0]
        param['buf_siz_hiword'] = struct.unpack('<H', fid.read(2))[0]
        param['cal_err_flag'] = struct.unpack('<H', fid.read(2))[0]
        param['system_type'] = struct.unpack('<H', fid.read(2))[0]
        param['cursors_moved'] = struct.unpack('<H', fid.read(2))[0]
        param['rean_hip_done'] = struct.unpack('<H', fid.read(2))[0]
        
        
        #skip header now and read data
        fid.seek(256)
        img_metal = numpy.empty([param['numLine']*param['numPts']])
        img_plastic = numpy.empty([param['numLine']*param['numPts']])
        img_type = numpy.empty([param['numLine']*param['numPts']])
        
        for i in range(param['numLine']*param['numPts']):
            img_metal[i] = struct.unpack('<h',fid.read(2))[0]
            tmp = struct.unpack('<h',fid.read(2))[0]
            img_plastic[i] = numpy.int16(numpy.uint16(tmp) & 0xfff0) #tmp & 0xfff0
            img_type[i] = numpy.uint16(tmp) & 0x000f
            
    
    img_metal = numpy.reshape(img_metal,[param['numLine'], param['numPts']])
    img_plastic = numpy.reshape(img_plastic,[param['numLine'], param['numPts']])
    img_type = numpy.reshape(img_type,[param['numLine'], param['numPts']])
    
    if img_id==0:
        return (img_metal,img_type,param)
    elif img_id==1:
        return (img_plastic,img_type,param)
    
    

def getPatientList(filename):

    with open(filename, mode='rb') as fid:
        out = []
        
        #Header
        #number of patients
        fid.seek(6)
        num_pat = struct.unpack('<H', fid.read(2))[0]
            
        
#        param['file_id'] = struct.unpack('<H', fid.read(2))[0]
#        param['minor_ver'] = struct.unpack('<B', fid.read(1))[0]
#        param['major_ver'] = struct.unpack('<B', fid.read(1))[0]

        #Patient data
        for ipat in range(num_pat):
            pPat = (ipat + 1) * 1024
            
            #Patient sequence number
            fid.seek(pPat + 14)
            seq_num = struct.unpack('<H', fid.read(2))[0]
            
            #Patient id
            fid.seek(pPat + 17)
            pat_id = ''
            for ilet in range(13):
                let = chr(struct.unpack('<b', fid.read(1))[0])
                if let=='\x00':
                    break
                pat_id += let
            
            #Patient name
            fid.seek(pPat + 30)
            pat_name = ''
            for ilet in range(23):
                let = chr(struct.unpack('<b', fid.read(1))[0])
                if let=='\x00':
                    break
                pat_name += let
            
            out.append([seq_num, pat_id, pat_name])
            
        return out
#        param['last_seq_no'] = struct.unpack('<H', fid.read(2))[0]
#        param['total_pats'] = struct.unpack('<H', fid.read(2))[0]
#        param['total_recs'] = struct.unpack('<H', fid.read(2))[0]
#        param['first_free'] = struct.unpack('<H', fid.read(2))[0]
#        param['first_id'] = struct.unpack('<H', fid.read(2))[0]
#        param['first_alpha'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_0'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_1'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_2'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_3'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_4'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_5'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_6'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_7'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_8'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_9'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_10'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_11'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_12'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_13'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_14'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_15'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_16'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_17'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_18'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_19'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_20'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_21'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_22'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_23'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_24'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_25'] = struct.unpack('<H', fid.read(2))[0]
#        param['alpha_26'] = struct.unpack('<H', fid.read(2))[0]
#        param['bsearch_name'] = struct.unpack('<H', fid.read(2))[0]
#        param['bsearch_id'] = struct.unpack('<H', fid.read(2))[0]
#        param['next_name'] = struct.unpack('<H', fid.read(2))[0]
#        param['prev_name'] = struct.unpack('<H', fid.read(2))[0]
#        param['next_id'] = struct.unpack('<H', fid.read(2))[0]
        
        