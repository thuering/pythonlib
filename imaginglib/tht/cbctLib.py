# -*- coding: utf-8 -*-
"""
Created on Mon Jun 06 11:48:09 2016

Helper functions for the CBCT image processing library

@author: tthuering
"""

## IMPORTS
import struct
import numpy as np
import os


## CLASSES

class DatasetParameters(object):
    width = 0
    height = 0
    nframes = 0
    fname = ""
    hSize = 0
    dataType = np.uint16
    
    def __init__(self, width=0, height=0, nframes=0, fname="", hSize=0, dataType=np.uint16):
        self.width = width
        self.height = height
        self.nframes = nframes
        self.fname = fname
        self.hSize = hSize
        self.dataType = dataType

## FUNCTIONS

def readSequenceFileParam(filename):
    filetype = filename[-3:]    
    if filetype == "mhd":
        return readMhdSequenceFileParam(filename)
        
    elif filetype == "seq":
        return readVarianSequenceFileParam(filename)    
    

# readMhdSequenceFileParam(filename)
#   read .mhd file parameters
def readMhdSequenceFileParam(filename):
    fid = open(filename, mode = 'r')
    
    # Extract header data    
    param = DatasetParameters(fname = filename[:-3] + "raw")
    header = fid.readlines() #get entire content
    fid.close()
    
    # Find DimSize
    tmp = [sStr for sStr in header if "DimSize" in sStr]
    if len(tmp) != 1:
        return -1
    strList = tmp[0].split('=')[1].strip().split()
    param.width, param.height, param.nframes = map(int, strList)
    
    # Find ElementType
    tmp = [sStr for sStr in header if "ElementType" in sStr]
    if len(tmp) != 1:
        return -1
    strTmp = tmp[0].split('=')[1].strip()
    if strTmp=="MET_FLOAT":
        param.dataType = np.float32
    elif strTmp=="MET_USHORT":
        param.dataType = np.uint16
    
    return param
    
    
# writeMhdFile(filename, dimX, dimY, dimZ, pixel_size)
 # Write Mhd File for specific raw file. Raw file must have the same name as 
 # specified filename for the mhd file
def writeMhdFile(filename, dimX, dimY, dimZ, pixel_size):
    fid_mhd = open(filename, 'w')
    fid_mhd.write("ObjectType = Image\n")
    fid_mhd.write("NDims = 3\n")
    fid_mhd.write("BinaryData = True\n")
    fid_mhd.write("BinaryDataByteOrderMSB = False\n")
    fid_mhd.write("CompressedData = False\n")
    fid_mhd.write("TransformMatrix = 1 0 0 0 1 0 0 0 1\n")
    fid_mhd.write("Offset = " + str(-float(dimX)/2 * pixel_size) + " " \
                              + str(-float(dimX)/2 * pixel_size) + " " \
                              + str(-float(dimY)/2 * pixel_size) + "\n")
    fid_mhd.write("CenterOfRotation = 0 0 0\n")
    fid_mhd.write("AnatomicalOrientation = RAI\n")
    fid_mhd.write("ElementSpacing = " + str(pixel_size) + " "\
                                      + str(pixel_size) + " "\
                                      + str(pixel_size) + "\n")
    fid_mhd.write("DimSize = " + str(dimX) + " " \
                               + str(dimY) + " " \
                               + str(dimZ) + "\n")
    fid_mhd.write("ElementType = MET_FLOAT\n")
    fid_mhd.write("ElementDataFile = " + os.path.basename(filename)[:-4] + ".raw\n")        
    
# readVarianSequenceFile(filename)
#   read Varian .seq file parameters. See Varian header specification (VivHdr.h) in PaxScan distribution folder
def readVarianSequenceFileParam(filename):
    
    fid = open(filename, mode = 'rb')
    
    # Header length check (byte zero)
    fid.seek(0)
    hlen = struct.unpack('<i',fid.read(4))[0] #read 4 byte integer
    
    if (hlen != 2048):
        return -1
    
    # Extract header data    
    param = DatasetParameters(fname = filename, hSize = 2048)
    
    #### Image Width
    fid.seek(16)
    param.width = struct.unpack('<i',fid.read(4))[0] #read 4 byte integer
    
    #### Image Height
    fid.seek(20)
    param.height = struct.unpack('<i',fid.read(4))[0] #read 4 byte integer
    
    #### Number of frames
    fid.seek(28)
    param.nframes = struct.unpack('<i',fid.read(4))[0] #read 4 byte integer
    
    fid.close()
    
#    # Extract image data
#    fid.seek(hlen)
#    img = np.empty((param.width,param.height), np.uint16)
#    img[:] = fid.read(param.width * param.height * param.nframes * 2)
    
    return param

#  writeItriParamFile(path, filepre, num_proj, max_angle=360.0, numdigits=4)
#  write Itri reconstruction parameter file
def writeItriParamFile(outpath,
                       datapath,
                       filepre,
                       num_cols,
                       num_rows,
                       num_proj,
                       max_angle,
                       pixel_size,
                       sod,
                       sid,
                       numdigits=4,
                       mode='recon'):

    outfile = outpath + '\\' + "DataScript_" + mode + '_' + filepre + ".txt"
    
    rpath = os.path.relpath(datapath, outpath)
    
    nx = int(num_cols) #number of columns [pixels]
    ny = int(num_rows) #number of rows [pixels]
    np = int(num_proj) #number of projections
    ma = max_angle #maximum angle [deg]
    ps = pixel_size #pixel size [mm]
    cx = int(num_cols/2.0) #image x-center [pixel]
    cy = int(num_rows/2.0) #image y-center [pixel]
    sod = int(sod) #source to object distance [mm]
    sid = int(sid) #source to detector distance [mm]
    
    interp = 1
    filt = 1
    
    # voxel volume calculation
    dx = cx - nx/2 #x-center displacement
    dy = cy - ny/2 #x-center displacement
    vs = float(sid)/float(sod) * ps #voxel size
    
    # write header
    fid = open(outfile, 'wb')
    fid.writelines([str(ny)+'\t',str(nx) + '\t' + str(np) + '\n'])
    fid.writelines([str(sod) + '\t', str(sid) + '\n'])
    fid.writelines([str(dx) + '\t', str(dy) + '\t', str(vs) + '\t', str(vs) + '\n'])
    fid.writelines([str(int(interp)) + '\n'])
    fid.writelines([str(ma) + '\t' + str(ma/np * interp) + '\n'])
    fid.writelines([str(filt) + '\n'])
    
    #write blank line
    fid.write('\n')
    
    #write file list
    for i in range(np):
        angle = i*ma/np
        if mode=='recon':
            filename = rpath + '/' + filepre + '_' + '%0*d' % (numdigits, i) + '.raw'
        elif mode=='calib':
            filename = datapath + '\\' + filepre + '_' + '%0*d' % (numdigits, i) + '.raw'
        if i==np-1:
            out_str = ['{0:.2f}'.format(angle) + '\t', filename]
        else:
            out_str = ['{0:.2f}'.format(angle) + '\t', filename + '\n']
            
        fid.writelines(out_str)
        
    fid.close()
        