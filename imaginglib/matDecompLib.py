# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 14:13:44 2017

@author: thomas.thuering

Library for material decomposition with spectral X-ray detectors.
Originally derived from SpyG_mammo_mat_decomp_functions.py (written by Spyridon Gkoumas)
"""

#!/usr/bin/python
from dectrislibCP import ImageIO as iio
import glob
import sys
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
import os
from scipy.interpolate import interp1d
import cv2

from imaginglib.tht import imgLib as iml

# Sum images inside a folder
def summed_img(path, thresh_no):
    flist = glob.glob(path + '*' + 'th_' + str(thresh_no) + '_' + '*.tif') # list of flat-field image names
    imgSum = iio.readImage(flist[0]) # read first image from the list
    imgSum.fill(0) # fill the first image read with zeros
    for lfile in flist:
        img = iio.readImage(lfile) # read each file inside the folder
        imgSum += img # add images inside the folder
    return imgSum

def Ebin_imgs(img_0, img_1):
	Ebin_0 = img_0*1.0 - img_1*1.0
	Ebin_1 = img_1*1.0
	return Ebin_0, Ebin_1	

def ff_corr_img(obj_img, flat_img):
	ff_corr_img_np = (obj_img * 1.0) / (flat_img * 1.0)
	return ff_corr_img_np

def interp_img(img_np, p_middle, left_edge, right_edge):
	#Iterate over vertical gap
	n = 0
	for (y,x), value in np.ndenumerate(img_np):
		if img_np[y,x]==0 or np.isnan(img_np[y,x]): # dead pixel or Gap
			n = n + 1
			if x == left_edge or x == right_edge:
				img_np[y,x]=img_np[y,x] # edge gap pixels
				#img_np[y,x]=img_np[y,x+p_edge] # edge gap pixels
				#img_np[y,x+2]=img_np[y,x+p_edge] # edge gap pixels
				
			else:
				try:
					img_np[y,x]=(img_np[y-p_middle,x]+img_np[y+p_middle,x])/2 # gap pixels
				#img_np[y,x+1]=(img_np[y,x-p_middle]+img_np[y,x+p_middle])/2 # edge pixels around gap
				#img_np[y,x-1]=(img_np[y,x-p_middle]+img_np[y,x+p_middle])/2 # edge pixels around gap
				#print "Game 1",n
				except:
					print("Boundaries hit ... (Vert.)")
					continue		
				#	#print "Game 2",n				
				#	#img_np[y,x]=img_np[y,x-p_edge]
	
	print("Interpolated pixels vert.: " + str(n))


	#Iterate 
	for (y,x), value in np.ndenumerate(img_np):
		if img_np[y,x]==0 or np.isnan(img_np[y,x]): # dead pixel or Gap
			n = n + 1
			try:
				img_np[y,x]=(img_np[y,x-p_middle]+img_np[y,x+p_middle])/2 # gap pixels
				#img_np[y+1,x]=(img_np[y-p_middle,x]+img_np[y+p_middle,x])/2 # edge pixels around gap
				#img_np[y-1,x]=(img_np[y-p_middle,x]+img_np[y+p_middle,x])/2 # edge pixels around gap
				
			except:
				print("Boundaries hit ... (Horiz.)")		
				continue
				#print("Intrpolated pixels horiz.: " + str(n))
				#try:
				#	img_np[y,x]=img_np[y-2,x] #should not execute	
				#except:
				#	img_np[y,x]=img_np[y+2,x] #should not execute
	
	print("Interpolated pixels Horiz.: " + str(n))
	return img_np

	
	
def interp_img_thot(img, border=2, maskMargin=1):
    img1 = np.copy(img)
    img1 = img1.astype(np.double)
     
    #get dead pixel mask
    mask1 = img1 == 0
     
    #extend mask by maskMargin
    mask2 = cv2.boxFilter(np.int32(mask1), ddepth=-1, ksize=(1+2*maskMargin,1+2*maskMargin), normalize=False, borderType=cv2.BORDER_REFLECT)
    mask3 = mask2 > 0
    #iml.imshow(mask3)
    
    #add border
    mask3[0:border,:] = 1
    mask3[-border:,:] = 1
    mask3[:,0:border] = 1
    mask3[:,-border:] = 1
          
    mask4 = ~mask3 #invert
    img1 = img1 * mask4
     
    #image and mask convolution with 3x3 kernel
    img1c = cv2.boxFilter(img1, ddepth=-1, ksize=(3+2*maskMargin,3+2*maskMargin), normalize=False, borderType=cv2.BORDER_REFLECT)
    maskc = cv2.boxFilter(np.int32(mask4), ddepth=-1, ksize=(3+2*maskMargin,3+2*maskMargin), normalize=False, borderType=cv2.BORDER_REFLECT)
    maskc[maskc==0] = 1
          
    #normalize corrected image
    img1c = img1c * 1/maskc
     
    #replace corrected values in gaps
    img_out = np.copy(img)
    img_out = img_out.astype(np.uint32)
    img_out[mask3] = img1c[mask3]
     
    return img_out	



