#!/usr/bin/python
import dectris.imaging.common.fileIO as dfi
import glob
import sys
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.image as mpimg
import os
from scipy.interpolate import interp1d


# Sum images inside a folder
def summed_img(path, thresh_no):
    imglist = glob.glob(path + '/series_id_00001_threshold_' + str(thresh_no) + '_frame_id_' + '*.tif') # list of flat-field image names
    imgSum = dfi.readImage(imglist[0]) # read first image from the list
    imgSum.fill(0) # fill the first image read with zeros
    for file in imglist:    
        img = dfi.readImage(file) # read each file inside the folder
        imgSum += img # add images inside the folder
    return imgSum / len(imglist)

def Ebin_imgs(img_0, img_1):
	Ebin_0 = img_0*1.0 - img_1*1.0
	Ebin_1 = img_1*1.0
	return Ebin_0, Ebin_1	

def ff_corr_img(obj_img, flat_img):
	ff_corr_img_np = (obj_img * 1.0) / (flat_img * 1.0)
	return ff_corr_img_np

def interp_img(img_np, p_middle, left_edge, right_edge):
	#Iterate over vertical gap
	n = 0
	for (y,x), value in np.ndenumerate(img_np):
		if img_np[y,x]==0 or np.isnan(img_np[y,x]): # dead pixel or Gap
			n = n + 1
			if x == left_edge or x == right_edge:
				img_np[y,x]=img_np[y,x] # edge gap pixels
				#img_np[y,x]=img_np[y,x+p_edge] # edge gap pixels
				#img_np[y,x+2]=img_np[y,x+p_edge] # edge gap pixels
				
			else:
				try:
					img_np[y,x]=(img_np[y-p_middle,x]+img_np[y+p_middle,x])/2 # gap pixels
				#img_np[y,x+1]=(img_np[y,x-p_middle]+img_np[y,x+p_middle])/2 # edge pixels around gap
				#img_np[y,x-1]=(img_np[y,x-p_middle]+img_np[y,x+p_middle])/2 # edge pixels around gap
				#print "Game 1",n
				except:
					print("Boundaries hit ... (Vert.)")
					continue		
				#	#print "Game 2",n				
				#	#img_np[y,x]=img_np[y,x-p_edge]
	
	print("Interpolated pixels vert.: " + str(n))
	
	#Iterate over horizontal gap
	n = 0
	for (y,x), value in np.ndenumerate(img_np):
		if img_np[y,x]==0 or np.isnan(img_np[y,x]): # dead pixel or Gap
			n = n + 1
			try:
				img_np[y,x]=(img_np[y,x-p_middle]+img_np[y,x+p_middle])/2 # gap pixels
				#img_np[y+1,x]=(img_np[y-p_middle,x]+img_np[y+p_middle,x])/2 # edge pixels around gap
				#img_np[y-1,x]=(img_np[y-p_middle,x]+img_np[y+p_middle,x])/2 # edge pixels around gap
				
			except:
				print("Boundaries hit ... (Horiz.)")		
				continue
				#print("Intrpolated pixels horiz.: " + str(n))
				#try:
				#	img_np[y,x]=img_np[y-2,x] #should not execute	
				#except:
				#	img_np[y,x]=img_np[y+2,x] #should not execute
	
	print("Interpolated pixels Horiz.: " + str(n))
	return img_np

	
	
	



