# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 15:15:29 2017

@author: thomas.thuering

Cross platform imports for image IO operations
"""

import platform
import os
import numpy as np

# Get OS information
osy = platform.system()
arch = platform.architecture()[0]

if (osy == 'Linux') or (osy == 'Windows' and arch == '32bit'):
    ioLib = 'skimage'

elif osy == 'Windows' and arch == '64bit':
    ioLib = 'libtiff'

#elif _platform == "darwin":
# MAC OS X

else:
    print('Error: OS not supported')


# Load modules according to OS information
if ioLib == 'albula':
    # linux
    import dectris.albula as dal
#    from dectris.albula import DImage
#    from dectris.albula import DImageWriter
#    from dectris.albula import readImage as 

elif ioLib == 'libtiff':
    from libtiff import TIFF
	
elif ioLib == 'skimage':
    import skimage.io as ski
    import warnings
    
else:
    print("OS not supported")
    
# ----------------------------------------------------------------------

# Functions

    
def readImage(a_filename):
    
    if ioLib == 'albula':
        return dal.readImage(a_filename).data()
    
    elif ioLib == 'libtiff':
        try:
            tif = TIFF.open(a_filename)
            img = tif.read_image()
            tif.close()
            return img
        except:
            print("ERROR: Could not find image: " + os.path.basename(a_filename))
    elif ioLib == 'skimage':
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            img = ski.imread(a_filename)
        return img
            
    
def writeImage(a_data, a_filename):
    if ioLib == 'albula':
        obj = dal.DImage()
        obj.fromData(a_data)
        dal.DImageWriter.write(a_filename)
        
    elif ioLib == 'libtiff':
        if not os.path.exists(os.path.dirname(a_filename)):
            os.makedirs(os.path.dirname(a_filename))
        tif = TIFF.open(a_filename, mode='w')
        
        if isinstance(a_data.flatten()[0], float):
            tif.write_image(a_data.astype(np.float32))
        elif isinstance(a_data.flatten()[0], int):
            tif.write_image(a_data.astype(np.uint32))
        else:
            print('Error: writeImage: unknown data type')
            
        tif.close()
        
    elif ioLib == 'skimage':
        dirName = os.path.dirname(a_filename)
        if not os.path.exists(os.path.dirname(a_filename)) and dirName != '':
            os.makedirs(os.path.dirname(a_filename))

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            if a_data.dtype == np.dtype('uint32') or a_data.dtype.type == np.uint32:
                ski.imsave(a_filename, a_data.astype("uint32"))
            elif a_data.dtype == np.dtype('float64') or a_data.dtype.type == np.float64:
                ski.imsave(a_filename, a_data.astype("float32"))
            else:
                ski.imsave(a_filename, a_data)
                
    else:
        print('Error: IO library not supported: ' + ioLib)


# Get IO library
def _getIoLib(osy, arch):
    if osy == 'Linux':
        return 'albula'
   
    elif osy == 'Windows' and arch == '32bit':
        return 'albula'
    
    elif osy == 'Windows' and arch == '64bit':
        return 'libtiff'
    
    #elif _platform == "darwin":
    # MAC OS X
    
    else:
        print('Error: OS not supported')
