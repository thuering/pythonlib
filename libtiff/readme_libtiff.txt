Found this library (pylibtiff) on http://www.lfd.uci.edu/~gohlke/pythonlibs/
Problem was that pylibtiff cannot be built on windows, so this is a pre-built library for windows.
Install package:
In Anaconda command prompt, run
pip install libtiff-0.4.0-cp27-none-win_amd64.whl


See these pages for the usage of this library:
http://tjelvarolsson.com/blog/saving-16bit-tiff-files-using-python/
http://www.aps.anl.gov/epics/tech-talk/2012/msg00616.php